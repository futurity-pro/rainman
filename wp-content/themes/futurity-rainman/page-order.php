<?php const CONSTRUCTOR_JEANS_ITEM_ID = 266;
global $woocommerce;
$woocommerce->cart->empty_cart();
$woocommerce->cart->add_to_cart( CONSTRUCTOR_JEANS_ITEM_ID );
?>

<?php get_header(); ?>
<!-- action="<?php the_permalink();?> -->

<form id="order" method="POST" action="/processing">
<h1>Оформление заказа</h1>

<div class="center">

<ul class="order-size tabs">
<li class="size-select">
<label><a class="size-select-type" href="standard-size">&#9654; Выбрать размер</a> <br><a class="size-select-type" href="custom-measure">&#9654; Снятие мерок</a></label>
<fieldset class="radio-type-size" style="display: none">
    <input type="radio" id="standard-size-radio" name="OrderForm[size-type]"  value="Размер" >
    <input type="radio" id="custom-measure-radio" checked="checked" name="OrderForm[size-type]" value="По мерке" >
</fieldset>
<script>
    $(function(){
        $('.size-select-type').click(function(e){
            e.preventDefault();
            $('.size-select-type').removeClass('active');
            $(this).addClass('active');
            var ShowContent = $(this).attr('href');

            // Hide or Show size field with pop-over
            if(ShowContent == 'standard-size'){
                $('#standard-size').show();
                $('.custom-measure input').removeClass('required');
                $('.custom-measure input').attr('disabled', true);
                $('.jeans-size-type-radio input').attr('disabled', false);
                $('.radio-type-size input').attr('disabled', false);
                $('#standard-size input').addClass('required');
                $('#standard-size input').attr('disabled', false);
                $('.popover-box').show();
            } else {
                $('#standard-size').hide();
                $('#standard-size input').removeClass('required');
                $('#standard-size input').attr('disabled', true);
                $('.popover-box').hide();

                $('.jeans-size-type-radio input').attr('disabled', true);
                $('.custom-measure').find('input').addClass('required');
                $('.custom-measure input').attr('disabled', false);

                $('#custom-measure-radio').attr('checked', 'checked');
                $('#standard-size-radio').attr('checked', false);

            }
            var checkedRadio = ShowContent + '-radio';

            $('.radio-type-size input').attr('checked',false);
            $('#'+checkedRadio).attr('checked', true);

            $('.size-type').hide();
            $('.' + ShowContent).show();

        }).click();

        var popoverBox = $('.popover-box');

        $('#standard-size').click(function(){
            if (popoverBox.is(':hidden')) {
                popoverBox.show();
            } else {
                popoverBox.hide();
            }
        });
        $('.jeans-sizes-type th').click(function(){
            $('.jeans-size tr td').removeClass('picked');
            $('.jeans-sizes-type th').removeClass('active');
            $(this).addClass('active');
            $('.jeans-size').hide();

            var tableID       = $(this).data('table');
            var sizeTypeRadio = $(this).data('table')+'-radio';

            $('.jeans-size-type-radio input').attr('checked', false);
            $(sizeTypeRadio).attr('checked', true);

            $(tableID).show();
            var td = $(tableID).find('tr').eq(1).children();
            $(td[0]).click();
            $(td[1]).click();

            return false;
        });
        $('.jeans-size tr td').click(function(){

            if($(this).hasClass('size')){
                $('.jeans-size tr .size').removeClass('picked');
                $(this).addClass('picked');
            }
            else if($(this).hasClass('inseam')){
                $('.jeans-size tr .inseam').removeClass('picked');
                $(this).addClass('picked');
            }
            var jeansSize = $('.jeans-size tr .size.picked').text();
            var jeansInseam = $('.jeans-size tr .inseam.picked').text();

            var sizeAndSeam = jeansSize + ' ' + jeansInseam;
            $('input[name="OrderForm[standard-size]"]').val(sizeAndSeam);
        });
        $('.jeans-sizes-type tr th:first-child').click();
    });
</script>
<div class="inputfield size-type standard-size"  id="standard-size" style="position: relative">
    <div class="bgactive">
        <input type="text" class="inpfield required"  name="OrderForm[standard-size]" data-label="Размер" value=""/>
        <span class="b1"></span><span id=""></span></div>
    <div class="clear"></div>

    <!-- Модальное окно с выбором размера-->
    <div class="popover-box">
        <div class="popover-box__inner">
            <i class="corner"></i>
            <div style="display: none" class="jeans-size-type-radio">
                <input type="radio" id="us-sizes-0-20-radio" name="OrderForm[size-standard]" value="USA Size 0-20"/>
                <input type="radio" id="us-sizes-20-32-radio" name="OrderForm[size-standard]" value="USA Size 32-32"/>
                <input type="radio" id="eu-sizes-radio"name="OrderForm[size-standard]" value="EU Size"/>
            </div>
            <table class="jeans-sizes-type">
                <thead>
                <tr>
                    <th data-table="#us-sizes-0-20" class="active">US sizes <br/> 0-20</th>
                    <th data-table="#us-sizes-20-32">US sizes <br/> 23-32</th>
                    <th data-table="#eu-sizes">EU sizes <br/> W / L</th>

                </tr>
                </thead>
            </table>
            <table width="100%" class="jeans-size" id="us-sizes-0-20">
                <tbody id="us-size">
                <tr>
                    <th width="85px">Размер</th>
                    <th>Inseam</th>
                </tr>
                <tr>
                    <td class="size picked" width="85px">00</td>
                    <td class="inseam">Extra short</td>
                </tr>
                <tr>
                    <td class="size" width="85px">0</td>
                    <td class="inseam picked">Short</td>
                </tr>
                <tr>
                    <td class="size" width="85px">2</td>
                    <td class="inseam">28</td>
                </tr>
                <tr>
                    <td class="size" width="85px">4</td>
                    <td class="inseam">30</td>
                </tr>
                <tr>
                    <td class="size" width="85px">6</td>
                    <td class="inseam" >Medium 32</td>
                </tr>
                <tr>
                    <td class="size" width="85px">8</td>
                    <td class="inseam" >Long 34</td>
                </tr>
                <tr>
                    <td class="size" width="85px">10</td>
                    <td class="inseam">Extra Long 36</td>
                    
                </tr>
                <tr>
                    <td class="size" width="85px">12</td>
                    <td class="inseam">Super Long 38</td>
                </tr>
                <tr>
                    <td class="size" width="85px">14</td>
                    <td class="inseam"></td>
                </tr>
            </table>
            <table width="100%" class="jeans-size" id="us-sizes-20-32">
                <tbody id="us-size">
                <tr>
                    <th width="85px">Талия</th>
                    <th>Inseam</th>
                </tr>
                <tr>
                    <td class="size picked" width="85px">23</td>
                    <td class="inseam" >Petite 28</td>
                </tr>
                <tr>
                    <td class="size" width="85px">24</td>
                </tr>

                <tr>
                    <td class="size" width="85px">25</td>
                    <td class="inseam" >Regular 30 </td>
                </tr>
                <tr>
                    <td class="size" width="85px">26</td>
                </tr>

                <tr>
                    <td class="size" width="85px">27</td>
                    <td class="inseam" >Long 32 </td>
                </tr>
                <tr>
                    <td class="size" width="85px">28</td>
                </tr>

                <tr>
                    <td class="size" width="85px">29</td>
                    <td class="inseam" >Long 34 </td>
                </tr>
                <tr>
                    <td class="size" width="85px">30</td>
                </tr>

                <tr>
                    <td class="size" width="85px">31</td>
                    <td class="inseam" >Long 36 </td>
                </tr>
                <tr>
                    <td class="size" width="85px">32</td>
                </tr>
                </tbody>
            </table>
            <table width="100%" class="jeans-size" id="eu-sizes">
                <tbody id="us-size">
                <tr>
                    <th width="85px">Талия</th>
                    <th>Inseam</th>
                </tr>

                <tr>
                    <td class="size picked" width="85px">W24</td>
                    <td class="inseam picked">L26</td>
                </tr>

                <tr>
                    <td class="size" width="85px">W25</td>
                    <td class="inseam">L27</td>
                </tr>
                <tr>
                    <td class="size" width="85px">W26</td>
                    <td class="inseam">L28</td>
                </tr>
                <tr>
                    <td class="size" width="85px">W27</td>
                    <td class="inseam">L30</td>
                </tr>
                <tr>
                    <td class="size" width="85px">W28</td>
                    <td class="inseam">L32</td>
                </tr>
                <tr>
                    <td class="size" width="85px">W29</td>
                    <td class="inseam">L34</td>
                </tr>
                <tr>
                    <td class="size" width="85px">W30</td>
                    <td class="inseam">L36</td>
                </tr>
                <tr>
                    <td class="size" width="85px">W31</td>
                    <td class="inseam">L38</td>
                </tr>
                <tr>
                    <td class="size" width="85px">W32</td>

                </tr>
                <tr>
                    <td class="size" width="85px">W33</td>

                </tr>
                <tr>
                    <td class="size" width="85px">W34</td>
                </tr>

                </tbody>
            </table>

        </div>
    </div>
    <!-- Модальное окно с выбором размера-->
</div>
</li>
</ul>
<div class="clear"></div>
<ul class="order-size tabs">
<?php for ($i = 1; $i <= 8; $i++): ?>
    <li class="size-type custom-measure">
        <a href="#">
            <div class="inputfield" >
                <label><?php echo synved_option_get('options', 'row'.$i.'label') ?></label>
                <div class="bgactive">
                    <input type="text" class="inpfield required" name="<?php echo "OrderForm[row$i]" ?>" data-label="<?php echo synved_option_get('options', 'row'.$i.'label') ?>" value=""/>
                    <span class="b1"></span><span id="bg<?php echo $i ?>"></span></div>
                <div class="clear"></div>
            </div>
        </a>
    </li>
<?php endfor; ?>
</ul>
<div class="panes">
    <?php for ($i = 1; $i <= 8; $i++): ?>
        <div>
            <div class="block-video">
                <div class="block-video-title"><span><?php echo synved_option_get('options', 'row'.$i.'header') ?></span></div>
                <div class="video">

                    <iframe id="player" type="text/html" width="492" height="300"
                            src="<?php echo synved_option_get('options', 'row'.$i.'video') ?>"
                            frameborder="0">
                    </iframe>

                </div>
            </div>
        </div>
    <?php endfor; ?>
</div>

<!--div class="price">Цена: <?php echo synved_option_get('options', 'price') ?> грн</div-->

<div class="order-form">
    <div class="form-row">
        <p>Прочитайте наши <a class="conditions" href="/conditions/">условия</a> работы</p>
        <input name="conditions" type="checkbox" class="required" data-label="Условия"/>
        <label>Я принимаю условия работы</label>
    </div>
    <div class="form-row-submit">
        <ul id="errors">
        </ul>
        <input type="submit" id="form-order" value="Оформить заказ"/>
        <div class="clear"></div>
    </div>
</div>

</div>
<div class="sidebar">
    <fieldset>
        <legend align="left">Модель</legend>
        <a href="/models/">
            <?php if(isset($_SESSION['model']['title'])): ?>
                <input type="hidden" class="required" name="OrderForm[model]" value="<?php echo $_SESSION['model']['title'] ?>">
                <?php echo $_SESSION['model']['title'] ?>
            <?php else: ?>
                <input type="hidden" class="required" name="model" value="">
                Выбрать модель
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Тип посадки</legend>
        <p><input type="radio" class="required" name="OrderForm[r1]" value="Высокая" data-label="Тип посадки"/><label>Высокая</label></p>
        <p><input type="radio" name="OrderForm[r1]" value="Средняя" data-label="Тип посадки"/><label>Средняя</label></p>
        <p><input type="radio" name="OrderForm[r1]" value="Низкая" data-label="Тип посадки"/><label>Низкая</label></p>
    </fieldset>
    <fieldset>
        <legend align="left">Ткань</legend>
        <a href="/cloth/">
            <?php if(isset($_SESSION['cloth']['title'])): ?>
                <input type="hidden" class="required" name="OrderForm[cloth]" data-label="Ткань" value="<?php echo $_SESSION['cloth']['title'] ?>">
                <?php echo $_SESSION['cloth']['title'] ?>
            <?php else: ?>
                <input type="hidden" class="required" name="OrderForm[cloth]" data-label="Ткань" value="">
                Выбрать ткань
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Нить</legend>
        <a href="/thread/">
            <?php if(isset($_SESSION['thread']['title'])): ?>
                <input type="hidden" name="OrderForm[cloth]" data-label="Нить" value="<?php echo $_SESSION['thread']['title'] ?>">
                <?php echo $_SESSION['thread']['title'] ?>
            <?php else: ?>
                <input type="hidden" class="required" name="OrderForm[cloth]" data-label="Нить" value="">
                Выбрать нить
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Механические изменения</legend>

            <?php if(isset($_SESSION['changes_type_rus'])): ?>

                <?php $damages = $_SESSION['changes_type_rus']; ?>
                <?php $damagesType = $_SESSION['changes_type']; ?>
                <?php $sessionPlace = $_SESSION['place'];?>
                <?php $places = '';?>
                <?php $placesText = array();?>
                <?php
                switch($damagesType){
                    case 'base': {
                        $placesText =  $_SESSION['changes_type_name'];
                        $places = $_SESSION['changes_type_name'];
                        break;
                    }

                    case 'local': {
                        foreach($sessionPlace as $place=>$name) {

                            $russianNamePlace = '';
                            switch($place){
                                case  'pocket':
                                    $russianNamePlace = 'Карман';
                                    break;

                                case  'thigh':
                                    $russianNamePlace = 'Бедро';
                                    break;
                                case  'knee':
                                    $russianNamePlace = 'Колено';
                                    break;
                                case  'bottom':
                                    $russianNamePlace = 'Низ Джинсов';
                                    break;
                            }
                            $places .= $russianNamePlace . ':' . $name . ' ';
                            array_push($placesText, '<p class="damage-place">' . $russianNamePlace . ', ' . $name . '</p>' );
                        }
                        $placesText = join('', $placesText);
                        break;
                    }


                }
                ?>
                <input type="hidden" name="OrderForm[damages]" data-label="Потёртость" value="<?php echo $damages . ': ' . $places ?>">
                <a href="/wears/">
                    <?php echo '<span class="order-title-local-changes">' . $damages .'</span>'?>
                </a>

                <div class="damage-description-box">
                    <?php echo $placesText ?>
                </div>
            <?php else: ?>
                <input type="hidden" class="required" name="OrderForm[damages]" data-label="Потёртость" value="">
                    <a href="/wears/">Выбрать механичекие изменения</a>
            <?php endif; ?>
        </a>
    </fieldset>
</div>
</form>
<?php get_footer() ?>
