<?php get_header() ?>
	<div class="ordering">
		<h1>Оформление заказа</h1>

<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $woocommerce;
$woocommerce->show_messages();
?>

<?php do_action( 'woocommerce_before_cart' ); ?>
<form action="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" method="post">
		<div class="inner-l">
			<form action="">
				<div class="form-box-short underline">

					<div class="box-title">
						Способ доставки
					</div>
					<div class="row">
						<input type="radio" id="courier" name="delivery-method"/>
						<label for="courier">Курьер</label>
					</div>
					<div class="row">
						<input type="radio" id="ukrpost" name="delivery-method"/>
						<label for="ukrpost">Укрпочта</label>
					</div>
					<div class="row">
						<input type="radio" id="newpost" name="delivery-method"/>
						<label for="newpost">Доставка Новой Почтой</label>
					</div>
					<div class="row">
						<input type="radio" id="dhl" name="delivery-method"/>
						<label for="dhl">Доставка DHL</label>
					</div>
				</div>

				<div class="form-box underline">
					<div class="box-title">
						Информация о получателе
					</div>
					<div class="row">
						<label for="firstname">Имя:</label>
						<input type="text" id="firstname" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="lastname">Фамилия:</label>
						<input type="text" id="lastname" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="patronymic">Отчество:</label>
						<input type="text" id="patronymic" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="phone">Телефон:</label>
						<input type="text" id="phone" name="delivery-method"/>
					</div>
				</div>

				<div class="form-box">
					<div class="box-title">
						Информация о получателе
					</div>
					<div class="row">
						<label for="city">Населенный пункт:</label>
						<input type="text" id="city" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="vilage">Улица:</label>
						<input type="text" id="vilage" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="house">Дом:</label>
						<input type="text" id="house" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="flat">Квартира:</label>
						<input type="text" id="flat" name="delivery-method"/>
					</div>
				</div>
				<input type="submit" id="order-submit" value="Оформить заказ"/>
			</form>
		</div>
		<div class="inner-r">

			<?//php do_action( 'woocommerce_before_cart_table' ); ?>

				<!--tr>
					<th class="product-remove">&nbsp;</th>
					<th class="product-thumbnail">&nbsp;</th>
					<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
					<th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
					<th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
					<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
				</tr-->
				<?//php do_action( 'woocommerce_before_cart_contents' ); ?>
				<?php
				if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
					foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
						$_product = $values['data'];
						if ( $_product->exists() && $values['quantity'] > 0 ) {
							?>
							<div class="order-item">
								<table>
									<tbody>
									<tr>
										<td>
											<div class="item-foto">
												<!-- The thumbnail -->
												<?php
												$thumbnail = apply_filters( 'woocommerce_in_cart_product_thumbnail', $_product->get_image(), $values, $cart_item_key );

												if ( ! $_product->is_visible() || ( ! empty( $_product->variation_id ) && ! $_product->parent_is_visible() ) )
													echo $thumbnail;
												else
													printf('<a href="%s">%s</a>', esc_url( get_permalink( apply_filters('woocommerce_in_cart_product_id', $values['product_id'] ) ) ), $thumbnail );
												?>
											</div>
										</td>

										<td>
											<div class="item-descr">
												<div class="item-title">
													<!-- Product Name -->
													<?php
													if ( ! $_product->is_visible() || ( ! empty( $_product->variation_id ) && ! $_product->parent_is_visible() ) )
														echo apply_filters( 'woocommerce_in_cart_product_title', $_product->get_title(), $values, $cart_item_key );
													else
														printf('<a href="%s">%s</a>', esc_url( get_permalink( apply_filters('woocommerce_in_cart_product_id', $values['product_id'] ) ) ), apply_filters('woocommerce_in_cart_product_title', $_product->get_title(), $values, $cart_item_key ) );

													// Meta data
													echo $woocommerce->cart->get_item_data( $values );

													// Backorder notification
													if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $values['quantity'] ) )
														echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';
													?>
												</div>

												<span class="item-size">22-37</span>

										<span class="item-num">

												<?php
											if ( $_product->is_sold_individually() ) {
												$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
											} else {

												$step	= apply_filters( 'woocommerce_quantity_input_step', '1', $_product );
												$min 	= apply_filters( 'woocommerce_quantity_input_min', '', $_product );
												$max 	= apply_filters( 'woocommerce_quantity_input_max', $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(), $_product );

												$q =   esc_attr( $values['quantity'] );

												echo '<select name="num">';
												for($i = 1; $i < $q+1; $i++){
													$selected = ($i==$q);
													if ($selected){
														echo '<option selected="selected" value="'. $i. '">' . $i .'</option>';
													}
													echo '<option  value="'. $i. '">' . $i .'</option>';
												}
												echo '</select>';
												//$product_quantity = sprintf( '<div class="quantity"><input  style="width: 72px; height: 25px;" type="number" name="cart[%s][qty]" step="%s" min="%s" max="%s" value="%s" size="4" title="' . _x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ) . '" class="input-text qty text" maxlength="12" /></div>', $cart_item_key, $step, $min, $max, esc_attr( $values['quantity'] ) );
											}
											echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
											?>

										</span>

										<span class="item-prize">
										<?php
											$product_price = get_option('woocommerce_tax_display_cart') == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();
											echo apply_filters('woocommerce_cart_item_price_html', woocommerce_price( $product_price *  esc_attr( $values['quantity'] )), $values, $cart_item_key );
											?>
										</span>
											</div>
										</td>

										<td>
											<div class="close">
												<!-- Remove from cart link -->
												<?php
												echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a style="display: block; width: 25px; height: 25px" href="%s" class="remove" title="%s"></a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
												?>
											</div>
										</td>
									</tr>
									</tbody
								</table>

							<!-- Product subtotal -->

									<!--?php
									echo apply_filters( 'woocommerce_cart_item_subtotal', $woocommerce->cart->get_product_subtotal( $_product, $values['quantity'] ), $values, $cart_item_key );
									?-->

						<?php
						}
					echo '</div>';
					}
				}

				do_action( 'woocommerce_cart_contents' );
				?>
				<tr>
					<td colspan="6" class="actions">

						<?php if ( $woocommerce->cart->coupons_enabled() ) { ?>
							<div class="coupon">

								<label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input name="coupon_code" class="input-text" id="coupon_code" value="" /> <input type="submit" class="button" name="apply_coupon" value="<?php _e( 'Apply Coupon', 'woocommerce' ); ?>" />

								<?php do_action('woocommerce_cart_coupon'); ?>

							</div>
						<?php } ?>

						<!--input type="submit" class="button" name="update_cart" value="<?php _e( 'Update Cart', 'woocommerce' ); ?>" /-->
						<input type="submit" class="checkout-button button alt" name="proceed" value="<?php _e( 'Proceed to Checkout &rarr;', 'woocommerce' ); ?>" />

						<?php do_action('woocommerce_proceed_to_checkout'); ?>

						<?php $woocommerce->nonce_field('cart') ?>
					</td>
				</tr>

				<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>


		<div class="cart-collaterals">

			<?php do_action('woocommerce_cart_collaterals'); ?>
			<?php woocommerce_cart_totals(); ?>
			<?php woocommerce_shipping_calculator(); ?>

		</div>

		<?php do_action( 'woocommerce_after_cart' ); ?>
			<div class="paiment-result">
				<p>
					Сумма к оплате: <?php echo  $woocommerce->cart->get_cart_subtotal();?>
				</p>
			</div>
		</div>
	</div>
</form>
<?php get_footer() ?>




