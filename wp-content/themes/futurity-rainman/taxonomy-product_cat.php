<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); ?>
<?php
global $woocommerce;
$itemCount = count($woocommerce->cart->get_cart());
foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {

	if( $cart_item['product_id'] == CONSTRUCTOR_JEANS_ITEM_ID ){
		unset($woocommerce->cart->cart_contents[$cart_item_key]);
	}

}

?>
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action('woocommerce_before_main_content');
	?>

		<?php /*if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; */?>

		<?php //do_action( 'woocommerce_archive_description' ); ?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

            <ul class="rain-man-jeans">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php //woocommerce_get_template_part( 'content', 'product' );
                        $image_id = get_post_thumbnail_id($product->id);
                        $url = wp_get_attachment_url( $image_id );
                        $urlThumb = wp_get_attachment_image_src( $image_id, 'large' );
                        $urlThumb = $urlThumb[0];
                        $attributes = $product->get_attributes();
                    ?>

                    <li style="float: left">
                        <img src="<?php echo $urlThumb;?>" alt="<?php echo $product->get_title(); ?>" height="468" width="312" />
                        <div class="shape-price">
                            <p><?php echo $product->get_price().' грн.'; ?></p>
                        </div>
                        <div class="description">
                            <div class="shape-title">
                                <p><a href="<?php echo get_permalink( $product->id );?>"><?php echo $product->get_title(); ?></a></p>
                            </div>
                            <div class="size">
                                <p>
                                    <?php foreach ($attributes as $attribute){
                                        $sizes = explode("|",$attribute['value']);
                                        $sizes_count = count($sizes);
                                        $first_size = $sizes[0];
                                        $last_size = end($sizes); ?>

                                        <?php if ($sizes_count>1){?>
                                            Размеры: <?php echo $first_size.'-'.$last_size?>
                                        <?php } else {?>
                                            Размеры: <?php echo $first_size?>
                                        <?php } ?>

                                    <?php }?>
                                </p>
                            </div>
                        </div>
                    </li>
				<?php endwhile; // end of the loop. ?>
            </ul>
			<div class="clear"></div>
			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action('woocommerce_sidebar');
	?>

<?php get_footer('shop'); ?>