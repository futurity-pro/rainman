<?php
unset($_SESSION['changes_type_name']);
unset($_SESSION['changes_type']);
unset($_SESSION['changes_type_rus']);
unset($_SESSION['place']);


if(isset($_GET['off'])) {
    unset ($_SESSION['damages']);
    header('location:/damages/');
    die;
}
if(is_archive() && isset($_SESSION['damages']['url'])) {
    header('location:'.$_SESSION['damages']['url']);
}
?>
<?php get_header() ?>

	<div id="wrapper">
		<ul class="demages-tabs tabs" id="demages-tabs">
			<li class="tab-select"><a href="#base-changes" class="">Базовые изменения</a></li>
			<li class="tab-select"><a href="#local-changes" class="current">Локальные изменения</a></li>
		</ul>
		<div class="demages-tabs panes">
			<div id="base-changes" style="display: none;">
				<div class="content">
					<div class="con-left">
						<h1>Выберите изменения</h1>
						<div class="clear"></div>
						<div class="gallery2">
							<ul class="gallery-list" id="gallery-list-basic">
								<?php query_posts('post_type=basic_changes'); while ( have_posts() ) : the_post(); ?>
									<li>
										<div class="gblock">
											<a href="<?php the_permalink() ?><?php if($current == $post->ID):?>?off<?php endif; ?>" ><?php the_post_thumbnail() ?><span class="<?php if($current == $post->ID):?>active<?php endif; ?>"></span></a>
										</div>
										<div class="li-bottom"><?php echo the_title() ?><!--a href="<?php the_permalink() ?>"></a--></div>
									</li>
								<?php endwhile; ?>
							</ul>

							<div class="clear"></div>

							<a href="/thread/" class="link1">Нити</a>
							<a href="/models/" class="link2">Модели</a>

							<div class="clear"></div>

							<div class="your-changes" id="your-changes-basic" style="display: none">
								<h1>Ваши изменения</h1>
								<ul id="change-set-box-basic"></ul>
							</div>
						</div>
					</div>
					<div class="con-right">
						<?php if (is_single()): $current = $post->ID; the_post(); ?>
							<div class="h1"><?php the_title() ?></div>
							<?php
							$_SESSION['damages'] = array(
								'title' => get_the_title(),
								'id' => $post->ID,
								'url' => get_permalink()
							);
							?>
							<div class="scroll-pane">
								<?php the_content() ?>
							</div>
							<div class="choice">
								<div class="choice-title">Вы выбрали потёртость.</div>
								<a href="/order/">Форма заказа</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div id="local-changes" style="display: block;">
				<div class="content">
					<div class="con-left">
						<h1>Выберите изменения</h1>

						<div class="gallery2">
							<ul class="gallery-list" id="gallery-list-local">
								<?php query_posts('post_type=local_changes'); while ( have_posts() ) : the_post(); ?>
									<li>
										<div class="gblock">
											<a href="<?php the_permalink() ?><?php if($current == $post->ID):?>?off<?php endif; ?>" ><?php the_post_thumbnail() ?><span class="<?php if($current == $post->ID):?>active<?php endif; ?>"></span></a>
										</div>
										<div class="li-bottom"><?php echo the_title()?> <!--a href="<?php the_permalink() ?>"><?php the_title() ?></a--></div>
									</li>
 								<?php endwhile; ?>
							</ul>
							<div class="clear"></div>

							<a href="/thread/" class="link1">Нити</a>
							<a href="/models/" class="link2">Модели</a>

							<div class="clear"></div>
							<div class="your-changes" id="your-changes-local" style="display: none">
								<h1>Ваши изменения</h1>
								<ul id="change-set-box-local"></ul>
							</div>
							<div class="clear"></div>

						</div>

					</div>
					<div class="con-right select-place">
						<h1>Выберите расположение</h1>
						<ul class="places-tabs tabs" id="places-tabs">
							<li><a href="#frontend" class="current">Вид спереди</a></li>
							<li><a href="#backend" class="">Вид сзади</a></li>
						</ul>
						<div class="places-tabs-panes panse">
							<div id="frontend" style="display: block;">
								<div class="jeans-frontend">
									<div class="jeans-photo"></div>
									<ul class="places" data-side="Перед джинсов">
										<li data-place="pocket"  data-rus-place="Карман" class="jeans-place pocket">Карман</li>
										<li data-place="thigh"   data-rus-place="Бедро" class="jeans-place thigh">Бедро</li>
										<li data-place="knee"    data-rus-place="Колено" class="jeans-place knee">Колено</li>
										<li data-place="bottom"  data-rus-place="Низ джинс" class="jeans-place bottom-jeans">Низ джинс</li>
									</ul>
								</div>
							</div>
							<div id="backend" style="display: none;">
								<div class="jeans-backend">
									<div class="jeans-photo"></div>
									<ul class="places" data-side="Зад джинсов">
										<li data-place="pocket"  data-rus-place="Карман" class="jeans-place pocket">Карман</li>
										<li data-place="thigh"   data-rus-place="Бедро" class="jeans-place thigh">Бедро</li>
										<li data-place="knee"    data-rus-place="Колено" class="jeans-place knee">Колено</li>
										<li data-place="bottom"  data-rus-place="Низ джинс" class="jeans-place bottom-jeans">Низ джинс</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
<div class="end-order-button">
    <div class="ffButtonWrapper ffSubmitWrapper">
        <a href="/order">
            <span>Завершить выбор</span>
        </a>
    </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#demages-tabs li').click(function(){
			$.ajax({
				'url' : '/constructor',
				'type' : 'POST',
				'data' : {
					'action' : 'session_remove_damages'
				}
			})
		})
		$('#frontend').show();
		$('.places .jeans-place').click(function(e){
			var place = $(this).data('place');
			var rusPlace = $(this).data('rus-place');
			var side = $(this).parent().data('side');

			$('.places .jeans-place').removeClass('active');
			$(this).addClass('active');
			$.ajax({
				'url' : '/constructor',
				'type' : 'POST',
				'data' : {
					'place' : place,
					'action' : 'session_write',
					'rus_place' : rusPlace,
					'side' : side
				},
				success : function(data){
					$('#your-changes-local').show();
					$('#change-set-box-local').append(data);
				}
			})
		});
		$('.remove-changes').live('click', function(e){
			var place = $(this).data('place');
			$('.places .jeans-place').removeClass('active');
			var box = $(this).parent();
			$(this).addClass('active');
			$.ajax({
				'url' : '/constructor',
				'type' : 'POST',
				'data' : {
					'place' : place,
					'action' : 'session_remove'
				},
				success : function(data){
					box.remove();
				}
			})
		});

		$('#gallery-list-basic li').click( function(e){
			e.preventDefault();
			var changeName =  $(this).text();

			$('#gallery-list-basic li a').removeClass('current');
			$(this).find('a').addClass('current');

			$.ajax({
				'url' : '/constructor',
				'type' : 'POST',
				'data' : {
					'action' : 'select_basic_change',
					'change_type_name' : changeName

				},

				success : function(data){
					$('#your-changes-basic').show();
					$('#change-set-box-basic').append(data);
                    $('#gallery-list-basic li > span').addClass("current");
				}
			})
		});

		$('#gallery-list-local li').click( function(e){
			e.preventDefault();

			var changeName =  $(this).text();

			$('#gallery-list-local li a').removeClass('current');
			$(this).find('a').addClass('current');

			$.ajax({
				'url' : '/constructor',
				'type' : 'POST',
				'data' : {
					'action' : 'select_local_change',
					'changes_type_name' : changeName

				},

				success : function(data){
					$('#your-changes-basic').show();
					$('#change-set-box-basic').append(data);
				}
			})
		});
	})
</script>
<?php get_footer() ?>
