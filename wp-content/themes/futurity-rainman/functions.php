<?php
//function formsubmit() {
//    $message = "<h1>Заказ на джинсы Rain Man</h1><br>
//    <br>
//    Имя: {$_POST['name']}<br>
//    Email: {$_POST['email']}<br>
//    Телефон: {$_POST['phone']}<br>
//    Комментарий:<br>
//    {$_POST['comments']}<br>
//    <br>
//    <b>Параметры заказа:</b><br>
//    Модель: {$_POST['model']}<br>
//    Тип посадки: {$_POST['r1']}<br>
//    Ткань: {$_POST['cloth']}<br>
//    Нить: {$_POST['thread']}<br>
//    Механические изменения: {$_POST['r2']}<br>
//    <br>
//    <b>Размеры:</b><br>";
//
//    for ($i=1; $i<=8; $i++) {
//        $message .= synved_option_get('options', 'row'.$i.'label');
//        $message .= ": ".$_POST['row'.$i]."<br>";
//    }
//
//    $headers  = 'MIME-Version: 1.0' . "\r\n";
//    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//    $headers .= 'From: Rain Man <order@rain-man.com.ua>' . "\r\n";
//
//    mail($_POST['email'], 'Rain Man: Новый заказ', $message, $headers);
//    mail(synved_option_get('options', 'email'), 'Rain Man: Новый заказ', $message, $headers);
//
//    $_SESSION = '';
//
//    header('location:/thankyou/');
//    die;
//}

function formsubmit() {

	if(!empty($_POST['OrderForm'])){
		$_SESSION['OrderForm'] = $_POST['OrderForm'];
		header('location:/checkout/');
		die();
	}
}


function rainmain_setup() {
	session_start();
	formsubmit();
	if (isset($_POST['name'])) {
		formsubmit();
	}

	load_theme_textdomain( 'rainman', get_template_directory() . '/languages' );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 236, 246, true );
	add_theme_support( 'woocommerce' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
	register_nav_menu( 'primary', __( 'Главное меню', 'rainman' ) );

	$options = array(
		'left' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Слева', 'rainman')
		),
		'right' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Справа', 'rainman')
		),
		'vkid' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('VK API ID', 'rainman')
		),
		'email' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Email для заказов', 'rainman')
		),
		/*'price' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Цена', 'rainman')
		),*/
		'vk' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('ВКонтакте', 'rainman')
		),
		'odnoklasniki' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Одноклассники', 'rainman')
		),
		'facebook' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Facebook', 'rainman')
		),
		'twitter' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Twitter', 'rainman')
		),
		'instagram' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Instagram', 'rainman')
		),
		'google' => array(
			'default' => '',
			'type' => 'text',
			'label' => __('Google+', 'rainman')
		),
		'model_image' => array(
			'type' => 'image',
			'label' => __('Изображение "Создай свой стиль"', 'rainman'),
			'tip' => __('Размером 235x246 px', 'rainman')
		),
		'model_description' => array(
			'type' => 'text',
			'label' => __('Описание "Создай свой стиль"', 'rainman'),
		),
		'merchant_id' => array(
			'type' => 'text',
			'label' => __('Liqpay Merchant ID', 'rainman'),
		),
		'merc_sign' => array(
			'type' => 'text',
			'label' => __('Liqpay Merchant Sign', 'rainman'),
		),
		'default_phone' => array(
			'type' => 'text',
			'label' => __('Liqpay Default Phone', 'rainman'),
		),
	);

	for ($i = 1; $i <= 8; $i++) {
		$options['row'.$i.'label'] = array(
			'default' => '',
			'type' => 'text',
			'label' => __('Параметр '.$i.'. Название', 'rainman')
		);
		$options['row'.$i.'header'] = array(
			'default' => '',
			'type' => 'text',
			'label' => __('Параметр '.$i.'. Заголовок', 'rainman')
		);
		$options['row'.$i.'video'] = array(
			'default' => '',
			'type' => 'text',
			'label' => __('Параметр '.$i.'. Видео', 'rainman')
		);
	}

	synved_option_register('options', $options);
}

add_action( 'after_setup_theme', 'rainmain_setup' );
function rainmain_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Виджеты', 'rainmain' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Виджеты появятся на страницах постов в правом столбике.', 'rainmain' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'rainmain_widgets_init' );

//WooCommerce

//adding custom currencies
add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {
	$currencies['ABC'] = __( 'Украинская гривна', 'woocommerce' );
	return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
	switch( $currency ) {
		case 'ABC': $currency_symbol = 'грн.'; break;
	}
	return $currency_symbol;
}

//remove breadcrumbs
function remove_woo_crumbs(){
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}
add_action('init','remove_woo_crumbs');

//remove sidebar
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

//remove result count
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

//remove catalog sorting
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

// display 6 products per page
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 6;' ), 20 );

//get rid of stupid tabs
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20, 2);

//get rid of related products
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

//remove product reviews
remove_action( 'woocommerce_product_tabs', 'woocommerce_product_reviews_tab', 30 );
remove_action( 'woocommerce_product_tab_panels', 'woocommerce_product_reviews_panel', 30 );

//custom add to cart messages
add_filter( 'woocommerce_add_to_cart_message', 'custom_add_to_cart_message' );
function custom_add_to_cart_message() {
	global $woocommerce;
	if (get_option('woocommerce_cart_redirect_after_add')=='yes') :
		$return_to = get_permalink(woocommerce_get_page_id('shop'));
		$message = sprintf('<a href="%s" class="button">%s</a> %s', $return_to, __('Продолжить покупку', 'woocommerce'), __('Товар добавлен в вашу корзину.', 'woocommerce') );
	else :
		$message = sprintf('<a href="%s" class="button">%s</a> %s', get_permalink(woocommerce_get_page_id('checkout')), __('Перейти на страницу заказа', 'woocommerce'), __('Товар добавлен в вашу корзину.', 'woocommerce') );
	endif;
	return $message;
}
// Add new post type for Cat upon WordPress initialization
add_action('init', 'basic_changes_init');
add_action('init', 'local_changes_init');
add_action('init', 'stock_front_init');
add_action('init', 'jeans_front_init');
remove_action( 'init', 'damages_init');

function basic_changes_init()
{
	//Labels for custom post type.
	$view_labels = array(

// general name for the post type, usually plural.
		'name' => _x('Баз. изменения', 'post type general name'),

//name for one object of this post type. Defaults to value of name
		'singular_name' => _x('basic_changes', 'post type singular name'),

//the all items text used in the menu. Default is the Name label
		'all_items' => __('Все Базовые изменения'),

// the add new text. The default is Add New for both hierarchical and non-hierarchical types. When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new' => _x('Добавить новое', 'basic_changes'),

// the add new item text. Default is Add New Post/Add New Page
		'add_new_item' => __('Добавить новое'),

// the edit item text. Default is Edit Post/Edit Page
		'edit_item' => __('Редактировать'),

//the new item text. Default is New Post/New Page
		'new_item' => __('Новое изменение'),

//the view item text. Default is View Post/View Page
		'view_item' => __('Посмотреть изменение'),

//the search items text. Default is Search Posts/Search Pages
		'search_items' => __('Искать изменение'),

//the not found text. Default is No posts found/No pages found
		'not_found' =>  __('Изменений не найдено'),

//the not found in trash text. Default is No posts found in Trash/No pages found in Trash
		'not_found_in_trash' => __('Изменений в корзине не найдено'),

//the parent text. This string isn't used on non-hierarchical types. In hierarchical ones the default is Parent Page
		'parent_item_colon' => ''
	);

//Arguments
	$args = array(

//A plural descriptive name for the post type marked for translation.
		'labels' => $view_labels,

//Whether a post type is intended to be used publicly either via the admin interface or by front-end users.
		'public' => true,

//Whether queries can be performed on the front end as part of parse_request().
		'publicly_queryable' => true,

//Whether to generate a default UI for managing this post type in the admin.
		'show_ui' => true,

//Sets the query_var key for this post type.
		'query_var' => true,

//Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
		'rewrite' => true,

//The string to use to build the read, edit, and delete capabilities. May be passed as an array to allow for alternative plurals when using this argument as a base to construct the capabilities
		'capability_type' => 'post',

//Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
		'hierarchical' => false,

//menu_position
		'menu_position' => 55,

//An alias for calling add_post_type_support() directly. As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),

//Enables post type archives. Will use $post_type as archive slug by default.
		'has_archive' => 'basic_changes'
	);
	register_post_type('basic_changes',$args);
}
function local_changes_init()
{
	//Labels for custom post type.
	$view_labels = array(

// general name for the post type, usually plural.
		'name' => _x('Лок. изменения', 'post type general name'),

//name for one object of this post type. Defaults to value of name
		'singular_name' => _x('local_changes', 'post type singular name'),

//the all items text used in the menu. Default is the Name label
		'all_items' => __('Все Локальные изменения'),

// the add new text. The default is Add New for both hierarchical and non-hierarchical types. When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new' => _x('Добавить новое', 'basic_changes'),

// the add new item text. Default is Add New Post/Add New Page
		'add_new_item' => __('Добавить новое'),

// the edit item text. Default is Edit Post/Edit Page
		'edit_item' => __('Редактировать'),

//the new item text. Default is New Post/New Page
		'new_item' => __('Новое изменение'),

//the view item text. Default is View Post/View Page
		'view_item' => __('Посмотреть изменение'),

//the search items text. Default is Search Posts/Search Pages
		'search_items' => __('Искать изменение'),

//the not found text. Default is No posts found/No pages found
		'not_found' =>  __('Изменений не найдено'),

//the not found in trash text. Default is No posts found in Trash/No pages found in Trash
		'not_found_in_trash' => __('Изменений в корзине не найдено'),

//the parent text. This string isn't used on non-hierarchical types. In hierarchical ones the default is Parent Page
		'parent_item_colon' => ''
	);

//Arguments
	$args = array(

//A plural descriptive name for the post type marked for translation.
		'labels' => $view_labels,

//Whether a post type is intended to be used publicly either via the admin interface or by front-end users.
		'public' => true,

//Whether queries can be performed on the front end as part of parse_request().
		'publicly_queryable' => true,

//Whether to generate a default UI for managing this post type in the admin.
		'show_ui' => true,

//Sets the query_var key for this post type.
		'query_var' => true,

//Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
		'rewrite' => true,

//The string to use to build the read, edit, and delete capabilities. May be passed as an array to allow for alternative plurals when using this argument as a base to construct the capabilities
		'capability_type' => 'post',

//Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
		'hierarchical' => false,

//menu_position
		'menu_position' => 54,

//An alias for calling add_post_type_support() directly. As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),

//Enables post type archives. Will use $post_type as archive slug by default.
		'has_archive' => 'local_changes'
	);
	register_post_type('local_changes',$args);
}

function stock_front_init()
{
	//Labels for custom post type.
	$view_labels = array(

// general name for the post type, usually plural.
		'name' => _x('Акции', 'post type general name'),

//name for one object of this post type. Defaults to value of name
		'singular_name' => _x('stock_front', 'post type singular name'),

//the all items text used in the menu. Default is the Name label
		'all_items' => __('Акции'),

// the add new text. The default is Add New for both hierarchical and non-hierarchical types. When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new' => _x('Добавить Акцию', 'stock_front'),

// the add new item text. Default is Add New Post/Add New Page
		'add_new_item' => __('Добавить новое'),

// the edit item text. Default is Edit Post/Edit Page
		'edit_item' => __('Редактировать'),

//the new item text. Default is New Post/New Page
		'new_item' => __('Новая акция'),

//the view item text. Default is View Post/View Page
		'view_item' => __('Посмотреть акцию'),

//the search items text. Default is Search Posts/Search Pages
		'search_items' => __('Искать акцию'),

//the not found text. Default is No posts found/No pages found
		'not_found' =>  __('Акций не найдено'),

//the not found in trash text. Default is No posts found in Trash/No pages found in Trash
		'not_found_in_trash' => __('Акций в корзине не найдено'),

//the parent text. This string isn't used on non-hierarchical types. In hierarchical ones the default is Parent Page
		'parent_item_colon' => ''
	);

//Arguments
	$args = array(

//A plural descriptive name for the post type marked for translation.
		'labels' => $view_labels,

//Whether a post type is intended to be used publicly either via the admin interface or by front-end users.
		'public' => true,

//Whether queries can be performed on the front end as part of parse_request().
		'publicly_queryable' => true,

//Whether to generate a default UI for managing this post type in the admin.
		'show_ui' => true,

//Sets the query_var key for this post type.
		'query_var' => true,

//Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
		'rewrite' => true,

//The string to use to build the read, edit, and delete capabilities. May be passed as an array to allow for alternative plurals when using this argument as a base to construct the capabilities
		'capability_type' => 'post',

//Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
		'hierarchical' => false,

//menu_position
		'menu_position' => 5,

//An alias for calling add_post_type_support() directly. As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),

//Enables post type archives. Will use $post_type as archive slug by default.
		'has_archive' => 'stock_front'
	);
	register_post_type('stock_front',$args);
}
function jeans_front_init()
{
	//Labels for custom post type.
	$view_labels = array(

// general name for the post type, usually plural.
		'name' => _x('Скидки', 'post type general name'),

//name for one object of this post type. Defaults to value of name
		'singular_name' => _x('jeans_front', 'post type singular name'),

//the all items text used in the menu. Default is the Name label
		'all_items' => __('Скидки на Джинсы'),

// the add new text. The default is Add New for both hierarchical and non-hierarchical types. When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
		'add_new' => _x('Добавить Скидку', 'jeans_front'),

// the add new item text. Default is Add New Post/Add New Page
		'add_new_item' => __('Добавить Скидку'),

// the edit item text. Default is Edit Post/Edit Page
		'edit_item' => __('Редактировать'),

//the new item text. Default is New Post/New Page
		'new_item' => __('Новая Скидка'),

//the view item text. Default is View Post/View Page
		'view_item' => __('Посмотреть Скидку'),

//the search items text. Default is Search Posts/Search Pages
		'search_items' => __('Искать Скидку'),

//the not found text. Default is No posts found/No pages found
		'not_found' =>  __('Скидок не найдено'),

//the not found in trash text. Default is No posts found in Trash/No pages found in Trash
		'not_found_in_trash' => __('Скидок в корзине не найдено'),

//the parent text. This string isn't used on non-hierarchical types. In hierarchical ones the default is Parent Page
		'parent_item_colon' => ''
	);

//Arguments
	$args = array(

//A plural descriptive name for the post type marked for translation.
		'labels' => $view_labels,

//Whether a post type is intended to be used publicly either via the admin interface or by front-end users.
		'public' => true,

//Whether queries can be performed on the front end as part of parse_request().
		'publicly_queryable' => true,

//Whether to generate a default UI for managing this post type in the admin.
		'show_ui' => true,

//Sets the query_var key for this post type.
		'query_var' => true,

//Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
		'rewrite' => true,

//The string to use to build the read, edit, and delete capabilities. May be passed as an array to allow for alternative plurals when using this argument as a base to construct the capabilities
		'capability_type' => 'post',

//Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
		'hierarchical' => false,

//menu_position
		'menu_position' => 5,

//An alias for calling add_post_type_support() directly. As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),

//Enables post type archives. Will use $post_type as archive slug by default.
		'has_archive' => 'jeans_front'
	);
	register_post_type('jeans_front',$args);
}
