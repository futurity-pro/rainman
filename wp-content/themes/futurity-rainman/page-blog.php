<?php get_header() ?>

<div class="wp-posts wp-column">
	<?php query_posts('type=post'); while(have_posts()): the_post(); ?>
		<div class="post">
			<div class="h1"><?php the_title() ?></div>
			<?php the_excerpt() ?>
			<div class="date">Posted on <?php the_date() ?> by Rain man</div>
			<!--a href="<?php the_permalink() ?>" class="more">(читать полностью...)</a-->
		</div>
	<?php endwhile; ?>
</div>

<div class="wp-widgets wp-column">
	<?php get_sidebar(); ?>
</div>
<?php get_footer() ?>