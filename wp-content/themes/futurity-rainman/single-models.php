<?php
if(isset($_GET['off'])) {
    unset ($_SESSION['model']);
    header('location:/model/');
    die;
}
if(is_archive() && isset($_SESSION['model']['url'])) {
    header('location:'.$_SESSION['model']['url']);
}
?>
<?php get_header() ?>
<div class="con-right">
    <?php if (is_single()): $current = $post->ID; the_post(); ?>
    <div class="h1"><?php the_title() ?></div>
    <?php
        $_SESSION['model'] = array(
            'title' => get_the_title(),
            'id' => $post->ID,
            'url' => get_permalink()
        );
    ?>
    <div class="scroll-pane">
        <?php the_content() ?>
    </div>
    <div class="choice">
        <div class="choice-title">Вы выбрали модель.
        Самое время <a href="/thread/">Выбрать нити</a></div>
        <a href="/order/">Форма заказа</a>
    </div>
    <?php endif; ?>
</div>
<div class="con-left">
    <h1>Выберите модель</h1>
    <div class="gallery2">
        <ul class="gallery-list">
            <?php query_posts('post_type=models'); while ( have_posts() ) : the_post(); ?>
            <li>
                <div class="gblock">
                    <a href="<?php the_permalink() ?><?php if($current == $post->ID):?>?off<?php endif; ?>" ><?php the_post_thumbnail() ?><span class="<?php if($current == $post->ID):?>active<?php endif; ?>"></span></a>
                </div>
                <div class="li-bottom"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></div>
            </li>
            <?php endwhile; ?>
        </ul>
        <a href="/thread/" class="link1">Нити</a>
        <a href="/cloth/" class="link2">Ткани</a>
    </div>
</div>
<?php get_footer() ?>