Rainman 1.0.1
================
![codeship](https://www.codeship.io/projects/88be84c0-7307-0131-b128-1eedf80e3085/status)
test CI: 3

* url: [rainman.dev](http://rainman.dev/)
* backend: [rainman.dev/wp-admin](http://rainman.dev/wp-admin/)
* user: admin
* password: admin

Requirements
------------

* [Git Bash](http://git-scm.com/downloads)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](http://www.vagrantup.com/)
* Ensure that `git config --global core.autocrlf false`
* Ensure you turned on `virtualization` option in your BIOS
* Restart PC after installing VirtualBox

Installation
------------

1. Clone this repo using `git clone git@bitbucket.org:futurity-pro/rainman.git` to your projects dir
2. Run `hosts.bat` using windows cmd.exe or double click on file in Windows Explorer
3. Run `vagrant up` usign Git Bash
4. Open http://tec-repair.dev/ in your browser

History Log
-----------

**1.0.1**

- Wordpress based shop